from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from sales_rest.models import Salesperson, Sale, Customer, AutomobileVO
from .encoders import (
    CustomerEncoder,
    SaleEncoder,
    SalespersonEncoder,
)


@require_http_methods(["DELETE", "GET", "POST"])
def api_salespeople(request, id=None):
    if request.method == "GET":
        try:
            salespersons = Salesperson.objects.all().order_by("last_name")
            return JsonResponse(
                {"salespersons": salespersons},
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            salesperson = Salesperson.objects.get(id=id)
            salesperson.delete()
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Exception:
            response = JsonResponse(
                {
                    "message": "Could not create the salesperson, try different employee id"
                }
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "POST", "DELETE"])
def api_customers(request, id=None):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse({"customers": customers}, encoder=CustomerEncoder)
    elif request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=id)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Exception:
            response = JsonResponse(
                {"message": "Could not create the customer"},
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "POST", "DELETE"])
def api_sales(request, id=None):
    if request.method == "GET":
        sale = Sale.objects.all()
        return JsonResponse({"sale": sale}, encoder=SaleEncoder, safe=False)
    elif request.method == "DELETE":
        try:
            sale = Sale.objects.get(id=id)
            sale.delete()
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            automobile_Vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=automobile_Vin)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile vin"},
                status=404,
            )
        try:
            sales_Person_Id = content["sales_person"]
            sales_Person = Salesperson.objects.get(id=sales_Person_Id)
            content["sales_person"] = sales_Person
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid salesperson id"},
                status=404,
            )
        try:
            customer_Id = content["customer"]
            customer = Customer.objects.get(id=customer_Id)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Invalid customer id"}, status=404)
        try:
            sale = Sale.objects.create(**content)
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False,
            )
        except Exception:
            response = JsonResponse(
                {
                    "message": "Could not create the sale, please verify the body data and try again"
                }
            )
            response.status_code = 400
            return response
