from django.contrib import admin
from sales_rest.models import Salesperson, AutomobileVO, Customer, Sale


# Register your models here.
# admin.site.register(Salesperson)
# admin.site.register(AutomobileVO)
# admin.site.register(Customer)
# admin.site.register(Sale)


@admin.register(Salesperson)
class SalespersonAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]


@admin.register(AutomobileVO)
class AutomobileVOAdmin(admin.ModelAdmin):
    list_display = ["id", "vin", "sold"]


@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = ["id", "first_name", "last_name", "address", "phone_number"]


@admin.register(Sale)
class SaleAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "automobile",
        "sales_person",
        "customer",
        "price",
    ]
