from django.urls import path
from .views import (
    api_salespeople,
    api_customers,
    api_sales,
)


urlpatterns = [
    path(
        "salespeople/",
        api_salespeople,
        name="api_salespeople",
    ),
    path(
        "salespeople/<int:id>/",
        api_salespeople,
        name="sales_person_delete",
    ),
    path(
        "customers/",
        api_customers,
        name="customers",
    ),
    path(
        "customers/<int:id>/",
        api_customers,
        name="customer_delete",
    ),
    path(
        "sales/",
        api_sales,
        name="api_sales",
    ),
    path(
        "sales/<int:id>/",
        api_sales,
        name="sales_delete",
    ),
]
