from django.urls import path
from .api_views import (
    api_list_created_appointments,
    api_appointment,
    api_list_technicians,
    api_technician,
    api_list_all_appointments,
)


urlpatterns = [
    path(
        "appointments/",
        api_list_created_appointments,
        name="api_list_created_appointments",
    ),
    path(
        "appointments/<int:appointmentId>/",
        api_appointment,
        name="api_appointment",
    ),
    path(
        "appointments/<int:appointmentId>/cancel/",
        api_appointment,
        name="api_appointment_cancel",
    ),
    path(
        "appointments/<int:appointmentId>/finish/",
        api_appointment,
        name="api_appointment_finish",
    ),
    path(
        "appointments/history/",
        api_list_all_appointments,
        name="api_list_all_appointments",
    ),
    path(
        "technicians/",
        api_list_technicians,
        name="api_list_technicians",
    ),
    path(
        "technicians/<int:technicianId>/",
        api_technician,
        name="api_technician",
    ),
]
