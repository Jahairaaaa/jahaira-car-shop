import React, {useEffect, useState} from 'react';


function MainPage() {
    const [automobiles, setAutomobiles] = useState([]);
    const [salesPeople, setSalesPeople] = useState([]);
    const [customers, setCustomers] = useState([]);

    const [automobile, setAutomobile] = useState('');
    const [salesPerson, setSalesPerson] = useState('');
    const [customer, setCustomer] = useState('');
    const [price, setPrice] = useState('');
    const [year, setYear] = useState([]);
    const [sale, setSales] = useState('');

    const getData = async () => {
        const automobilesUrl = `http://localhost:8100/api/automobiles/`;
        const automobilesResponse = await fetch(automobilesUrl);
        const salesPeopleUrl = `http://localhost:8090/api/salespeople/`;
        const salesPeopleResponse = await fetch(salesPeopleUrl);
        const customersUrl = `http://localhost:8090/api/customers/`;
        const customersResponse = await fetch(customersUrl);

        if (automobilesResponse.ok) {
            const automobilesData = await automobilesResponse.json();
            setAutomobiles(automobilesData.autos);
        } else {
            console.error('An error occurred while fetching the automobiles data:', automobilesResponse.statusText, automobilesResponse.status);
        }

        if (salesPeopleResponse.ok) {
            const salesPeopleData = await salesPeopleResponse.json();
            setSalesPeople(salesPeopleData.salespersons);
        } else {
            console.error('An error occurred while fetching the salespeople data:', salesPeopleResponse.statusText, salesPeopleResponse.status);
        }

        if (customersResponse.ok) {
            const customersData = await customersResponse.json();
            setCustomers(customersData.customers);
        } else {
            console.error('An error occurred while fetching the customers data:', customersResponse.statusText, customersResponse.status);
        }
    }

    const putAutoSold = async (vin) => {
        const putBody = { "sold": true };
        const automobileUrl = `http://localhost:8100/api/automobiles/${vin}/`;
        const fetchConfig = { method:"put", body: JSON.stringify(putBody), headers: {'Content-Type': 'application/json'} };
        const automobileUpdateResponse = await fetch(automobileUrl, fetchConfig);
        if (automobileUpdateResponse.ok) {
            alert("Automobile sold attribute updated to true!");
        } else {
            console.error('put error:'. automobileUpdateResponse.statusText, automobileUpdateResponse.status);
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/salespeople/';
        const autoUrl = 'http://localhost:8100/api/automobiles/';
        const customerUrl = 'http://localhost:8090/api/customers/';

        const response = await fetch(url);
        const autoResponse = await fetch(autoUrl);
        const customerResponse = await fetch(customerUrl);
        if (response.ok) {
            const data = await response.json();
            setSalesPeople(data.salespersons);
        }
        if (autoResponse.ok) {
            const autoData = await autoResponse.json();
            setAutomobiles(autoData.autos);
        }
        if (customerResponse.ok) {
            const customerData = await customerResponse.json();
            setCustomers(customerData.customers);
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
            data.automobile= automobile;
            data.sales_person = salesPerson;
            data.customer = customer;
            data.price = price;
        const salesUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(salesUrl, fetchConfig);
        if (response.ok) {
            const newSale = await response.json();
            setAutomobile('');
            setSalesPerson('');
            setCustomer('');
            setPrice('');
            setYear('');
            setSales('');
            alert('sale successfully recorded');
            putAutoSold(newSale.automobile.vin);
        } else {
            console.error('error:', response.statusText, response.status, "if you just created automobile, wait for 1 min for polling, click Create again!");
            alert("If you just created the automobile, wait for 1 min for polling, click Create again!");
        }
    }

    useEffect(() =>{
        fetchData();
        getData();
    }, []);

    const handleAutomobileChange = (event) => {
        const value = event.target.value;
        setAutomobile(value);
    }
    const handleSalesPersonChange = (event) => {
      const value = event.target.value;
      setSalesPerson(value);
  }

  const handleCustomerChange = (event) => {
      const value = event.target.value;
      setCustomer(value);
  }

  const handlePriceChange = (event) => {
      const value = event.target.value;
      setPrice(value);
  }

 const handleYearChange = (event) => {
  const value = event.target.value;
  setYear(value);
 }

    const filteredAutomobiles = automobiles.filter(automobile => automobile.sold === false);
    const allYears = automobiles.map(automobile => automobile.year);
    const uniqueYears = [...new Set(allYears)];

  return (
    <>
    <div>
      <div id="carouselExampleDark" className="carousel carousel-dark slide" data-bs-ride="true">
      <div className="carousel-indicators">
        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
      </div>
    <div className="carousel-inner">
      <div className="carousel-item active">
        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ7EWcn-anOx95ULSjlXEBSXPYWALt0GAeC9w&usqp=CAU" className="d-block w-100" alt="..." style={{ width: '100%', height: '250px'}} />
        <div className="carousel-caption d-none d-md-block">
          <h2 className="text-white">Jahaira's Car Shop</h2>
          <h5 className="text-white">The Best Lamborghini's in the World</h5>
          <p>You don't want to miss these new models!</p>
        </div>
      </div>
      <div className="carousel-item">
        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ7EWcn-anOx95ULSjlXEBSXPYWALt0GAeC9w&usqp=CAU" className="d-block w-100" alt="..." style={{ width: '100%', height: '250px'}}/>
        <div className="carousel-caption d-none d-md-block">
          <h5>Second slide label</h5>
          <p>Some representative placeholder content for the second slide.</p>
        </div>
      </div>
      <div className="carousel-item">
        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ7EWcn-anOx95ULSjlXEBSXPYWALt0GAeC9w&usqp=CAU" className="d-block w-100" alt="..." style={{ width: '100%', height: '250px'}}/>
        <div className="carousel-caption d-none d-md-block">
          <h5>Third slide label</h5>
          <p>Some representative placeholder content for the third slide.</p>
        </div>
      </div>
    </div>
    <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
      <span className="carousel-control-prev-icon" aria-hidden="true"></span>
      <span className="text-body">Previous</span>
    </button>
    <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
      <span className="carousel-control-next-icon" aria-hidden="true"></span>
      <span className="text-body">Next</span>
    </button>
  </div>
  </div>

{/* 
    <div style={{ backgroundColor: "rgba(282, 182, 182, .8)" }} className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">Jahaira's Car Shop</h1>
      <div className="col-sm-6 mx-auto">
        <p className="lead mb-4">
          <b>
            The premiere solution for automobile dealership
            management!
          </b>
          <img src="https://pics.craiyon.com/2023-10-04/ccb068b4562443658e5a4fca84e55f6a.webp" alt="Logo" height={200} width={200} />
        </p>
      </div> */}







    {/* </div> */}
      <h2>Automobiles for Sale</h2>
      <br></br>
      <div style={{ backgroundColor: "white" }}>
        <h6>
          Auto Filter <br></br>
          <select onChange={handleAutomobileChange} value={automobile} required name="automobile" id="automobile">
            <option value="">Make</option>
            {filteredAutomobiles.map(automobile => {
                return (
                    <option key={automobile.href} value={automobile.vin}>{automobile.model.manufacturer.name}</option>
                    );
            })}
        </select>
        <select onChange={handleYearChange} value={year} required name="year" id="year">
            <option value="">Year</option>
            {uniqueYears.map(uniqueYear => {
                return (
                    <option key={uniqueYear} value={uniqueYear}>{uniqueYear}</option>
                    );
            })}
        </select>
        </h6>
      </div>
      <br></br>
      <div className="row row-cols-3 row-cols-sm-3 g-4">
        {filteredAutomobiles.map(automobile => (
        <div className="col-sm-6 mb-3" key={automobile.vin}>
        <div className="card shadow p-3 mb-5 bg-body-tertiary rounded">
          <img src="https://pics.craiyon.com/2023-10-04/ccb068b4562443658e5a4fca84e55f6a.webp" className="card-img-top" style={{width: "50%"}} alt="Car image"/>
          <div className="card-body">
            <h5 className="card-title">{automobile.model.name}</h5>
          </div>
        </div>
        </div>
        ))}
        </div>
        


    </>
  );
}

export default MainPage;
