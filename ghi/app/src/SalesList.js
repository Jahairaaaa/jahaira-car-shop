import React, { useEffect, useState } from 'react';


function SalesList() {

    const [sales, setSales] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/sales/');
        if (response.ok) {
            const salesData = await response.json();
            setSales(salesData.sale);
        } else {
            console.error('An error occurred fetching the data');
        }
    };

    useEffect(() => {
        getData();
    }, []);

    return (
        <div style={{backgroundColor: "rgba(282, 182, 182, .5)"}} className="container">
            <div className="my-5 container">
                <div className="row">
                    <h1>Sales</h1>
                    <table className="table table-striped m-3">
                        <thead style={{boxSizing:'content-box', height:'80px'}}>
                            <tr>
                                <th>Employee Name</th>
                                <th>Employee ID</th>
                                <th>Customer</th>
                                <th>Price</th>
                                <th>Vin</th>
                            </tr>
                        </thead>
                        <tbody>
                            {sales?.map(sale => {
                                return (
                                    <tr key={sale.href} value={sale.id}>
                                        <td>{sale.sales_person.first_name}</td>
                                        <td>{sale.sales_person.employee_id}</td>
                                        <td>{sale.customer.first_name}</td>
                                        <td>{sale.price}</td>
                                        <td>{sale.automobile.vin}</td>
                                    </tr>
                            )})}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}

export default SalesList;
