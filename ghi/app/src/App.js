import { BrowserRouter, Routes, Route } from 'react-router-dom'
import MainPage from './MainPage'
import Nav from './Nav'
import CustomerForm from './CustomerForm'
import CustomersList from './CustomersList'
import SalesList from './SalesList'
import SalesForm from './SalesForm'
import SalesPersonForm from './SalesPersonForm'
import SalesPeopleList from './SalesPeopleList'
import SalesHistory from './SalesHistory'
import TechnicianForm from './serviceComponents/TechnicianForm'
import TechniciansList from './serviceComponents/TechnicianList'
import ServiceForm from './serviceComponents/ServiceForm'
import ServicesList from './serviceComponents/ServiceList'
import ServiceHistory from './serviceComponents/ServiceHistory'
import ManufacturersList from './inventoryComponents/ManufacturersList'
import ManufacturerForm from './inventoryComponents/ManufacturerForm'
import ModelsList from './inventoryComponents/ModelsList'
import ModelForm from './inventoryComponents/ModelForm'
import AutomobilesList from './inventoryComponents/AutomobilesList'
import AutomobileForm from './inventoryComponents/AutomobileForm'



function App() {
  return (
    <BrowserRouter>
      <Nav />
        <Routes>

          <Route path="/" element={<MainPage />} />

          <Route path="/manufacturers/" element={<ManufacturersList />} />
          <Route path="/manufacturers/new/" element={<ManufacturerForm />} />
          <Route path="/models/" element={<ModelsList />} />
          <Route path="/models/new/" element={<ModelForm />} />
          <Route path="/automobiles/" element={<AutomobilesList />} />
          <Route path="/automobiles/new/" element={<AutomobileForm />} />

          <Route path="/salespeople/" element={<SalesPeopleList />} />
          <Route path="/salespeople/new/" element={<SalesPersonForm />} />
          <Route path="/customers/" element={<CustomersList />} />
          <Route path="/customers/new/" element={<CustomerForm />} />
          <Route path="/sales/" element={<SalesList />} />
          <Route path="/sales/new/" element={<SalesForm />} />
          <Route path="/sales/history/" element={<SalesHistory />} />

          <Route path="/technicians/" element={<TechniciansList />} />
          <Route path="/technicians/new/" element={<TechnicianForm />} />
          <Route path="/services/" element={<ServicesList />} />
          <Route path="/services/new/" element={<ServiceForm />} />
          <Route path="/services/history/" element={<ServiceHistory />} />
        </Routes>
    </BrowserRouter>
  )
}

export default App
