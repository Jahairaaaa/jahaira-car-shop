import React, {useState, useEffect} from 'react';


function ManufacturersList() {

    const [manufacturers, setManufacturers] = useState([]);

    const getManufacturersData = async () => {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        if (response.ok) {
            const manufacturersData = await response.json();
            setManufacturers(manufacturersData.manufacturers);
        } else {
            console.error("An error occurred while fetching the data", response.statusText, response.status);
        }
    }

    useEffect(() => {
        getManufacturersData();
    }, []);

    return (
        <div style={{backgroundColor: "rgba(282, 182, 182, .8)"}} className="container">
            <div className="my-5 container">
                <div className="row">
                    <h1 style={{textAlign: 'var(--bs-body-text-align)'}}>Manufacturers</h1>
                    <table className="table table-striped">
                        <thead style={{boxSizing:'content-box', height:'80px'}}>
                            <tr>
                                <th>Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            {manufacturers.map(manufacturer => {
                                return (
                                    <tr key={ manufacturer.href }>
                                        <td>{ manufacturer.name }</td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

export default ManufacturersList;
