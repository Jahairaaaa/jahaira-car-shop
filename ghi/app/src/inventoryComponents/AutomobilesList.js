import React, {useState, useEffect} from 'react'


function AutomobilesList() {

    const [automobiles, setAutomobiles] = useState([])

    const getAutomobilesData = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/')
        if (response.ok) {
            const automobilesData = await response.json()
            setAutomobiles(automobilesData.autos)
        } else {
            console.error("An error occurred while fetching the data", response.statusText, response.status)
        }
    }

    useEffect(() => {
        getAutomobilesData()
    }, [])

    return (
        <div style={{backgroundColor: "rgba(282, 182, 182, .8)"}} className="container">
            <div className="my-5 container">
                <div className="row">
                    <h1 style={{textAlign: 'var(--bs-body-text-align)'}}>Automobiles</h1>
                    <table className="table table-striped">
                        <thead style={{boxSizing:'content-box', height:'80px'}}>
                            <tr>
                                <th>VIN</th>
                                <th>Color</th>
                                <th>Year</th>
                                <th>Model</th>
                                <th>Manufacturer</th>
                                <th>Sold</th>
                            </tr>
                        </thead>
                        <tbody>
                            {automobiles.map(automobile => {
                                return (
                                    <tr key={ automobile.href }>
                                        <td>{ automobile.vin }</td>
                                        <td>{ automobile.color }</td>
                                        <td>{ automobile.year }</td>
                                        <td>{ automobile.model.name }</td>
                                        <td>{ automobile.model.manufacturer.name }</td>
                                        <td>{ automobile.sold?'Yes':'No' }</td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

export default AutomobilesList
