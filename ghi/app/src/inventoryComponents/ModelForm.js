import React, { useEffect, useState } from 'react'


function ModelForm () {

    const [manufacturers, setManufacturers] = useState([])

    const [name, setName] = useState('')
    const [picture_url, setPictureUrl] = useState('')
    const [manufacturer, setManufacturer] = useState('')

    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }

    const handlePictureUrlChange = (event) => {
        const value = event.target.value
        setPictureUrl(value)
    }

    const handleManufacturerChange = (event) => {
        const value = event.target.value
        setManufacturer(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.name = name
        data.picture_url = picture_url
        data.manufacturer_id = manufacturer
        const modelsUrl = `http://localhost:8100/api/models/`
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {'Content-Type': 'application/json',}
        }
        const newModelResponse = await fetch(modelsUrl, fetchConfig)
        if (newModelResponse.ok) {
            setName('')
            setPictureUrl('')
            setManufacturer('')
            alert("Manufacturer successfully added!")
        } else {
            console.error('error:', newModelResponse.statusText, newModelResponse.status, "most likely an error with the picture_url, please verify and resubmit!")
            alert('Please verify and correct the picture URL')
        }
    }

    const fetchData = async () => {
        const manufacturersUrl = `http://localhost:8100/api/manufacturers/`
        const response = await fetch(manufacturersUrl)
        if (response.ok) {
            const manufacturersData = await response.json()
            setManufacturers(manufacturersData.manufacturers)
        }
    }

    useEffect(() =>{
        fetchData()
    }, [])

    let dropdownClasses = 'form-select d-none'
    if (manufacturers.length > 0) {
      dropdownClasses = 'form-select'
    }

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div style={{backgroundColor: "rgba(282, 182, 182, .5)"}} className="shadow p-4 mt-4">
                        <h1>Create a vehicle model</h1>
                        <form onSubmit={handleSubmit} id="create-model-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleNameChange} value={name} placeholder="name" required type="text" name="name" id="name" className="form-control"/>
                                <label htmlFor="name">Model name...</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handlePictureUrlChange} value={picture_url} placeholder="picture_url" required type="text" name="picture_url" id="picture_url" className="form-control"/>
                                <label htmlFor="picture_url">Picture Url...</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={handleManufacturerChange} value={manufacturer} required name="manufacturer" id="manufacturer" className={dropdownClasses}>
                                    <option value="">Choose a manufacturer...</option>
                                    {manufacturers.map(manufacturer => {
                                        return (
                                            <option key={manufacturer.href} value={manufacturer.id}>{manufacturer.name}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ModelForm
