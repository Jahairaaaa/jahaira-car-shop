import React, {useState, useEffect} from 'react'


function ModelsList() {

    const [models, setModels] = useState([])

    const getModelsData = async () => {
        const response = await fetch('http://localhost:8100/api/models/')
        if (response.ok) {
            const modelsData = await response.json()
            setModels(modelsData.models)
        } else {
            console.error("An error occurred while fetching the data", response.statusText, response.status)
        }
    }

    useEffect(() => {
        getModelsData()
    }, [])

    return (
        <div style={{backgroundColor: "rgba(282, 182, 182, .8)"}} className="container">
            <div className="my-5 container">
                <div className="row">
                    <h1 style={{textAlign: 'var(--bs-body-text-align)'}}>Models</h1>
                    <table className="table table-striped">
                        <thead style={{boxSizing:'content-box', height:'80px'}}>
                            <tr>
                                <th>Name</th>
                                <th>Manufacturer</th>
                                <th>Picture</th>
                            </tr>
                        </thead>
                        <tbody>
                            {models.map(model => {
                                return (
                                    <tr key={ model.href }>
                                        <td>{ model.name }</td>
                                        <td>{ model.manufacturer.name }</td>
                                        <td><img style={{width: "150px"}} alt={`${model.manufacturer.name} ${model.name} model`} src={ model.picture_url }/></td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

export default ModelsList
