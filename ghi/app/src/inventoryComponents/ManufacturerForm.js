import React, { useState } from 'react'


function ManufacturerForm () {

    const [name, setName] = useState('')

    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.name = name

        const manufacturersUrl = `http://localhost:8100/api/manufacturers/`
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {'Content-Type': 'application/json',}
        }
        const newManufacturerResponse = await fetch(manufacturersUrl, fetchConfig)
        if (newManufacturerResponse.ok) {
            setName('')
            alert("Manufacturer successfully added!")
        } else {
            console.error('error:', newManufacturerResponse.statusText, newManufacturerResponse.status)
        }
    }

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div style={{backgroundColor: "rgba(282, 182, 182, .5)"}} className="shadow p-4 mt-4">
                        <h1>Create a manufacturer</h1>
                        <form onSubmit={handleSubmit} id="create-manufacturer-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleNameChange} value={name} placeholder="name" required type="text" name="name" id="name" className="form-control"/>
                                <label htmlFor="name">Manufacturer name...</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ManufacturerForm
