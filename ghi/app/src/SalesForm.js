import React, {useEffect, useState} from 'react';


function SalesForm() {

    const [automobiles, setAutomobiles] = useState([]);
    const [salesPeople, setSalesPeople] = useState([]);
    const [customers, setCustomers] = useState([]);

    const [automobile, setAutomobile] = useState('');
    const [salesPerson, setSalesPerson] = useState('');
    const [customer, setCustomer] = useState('');
    const [price, setPrice] = useState('');

    const getData = async () => {
        const automobilesUrl = `http://localhost:8100/api/automobiles/`;
        const automobilesResponse = await fetch(automobilesUrl);
        const salesPeopleUrl = `http://localhost:8090/api/salespeople/`;
        const salesPeopleResponse = await fetch(salesPeopleUrl);
        const customersUrl = `http://localhost:8090/api/customers/`;
        const customersResponse = await fetch(customersUrl);

        if (automobilesResponse.ok) {
            const automobilesData = await automobilesResponse.json();
            setAutomobiles(automobilesData.autos);
        } else {
            console.error('An error occurred while fetching the automobiles data:', automobilesResponse.statusText, automobilesResponse.status);
        }

        if (salesPeopleResponse.ok) {
            const salesPeopleData = await salesPeopleResponse.json();
            setSalesPeople(salesPeopleData.salespersons);
        } else {
            console.error('An error occurred while fetching the salespeople data:', salesPeopleResponse.statusText, salesPeopleResponse.status);
        }

        if (customersResponse.ok) {
            const customersData = await customersResponse.json();
            setCustomers(customersData.customers);
        } else {
            console.error('An error occurred while fetching the customers data:', customersResponse.statusText, customersResponse.status);
        }
    }

    const putAutoSold = async (vin) => {
        const putBody = { "sold": true };
        const automobileUrl = `http://localhost:8100/api/automobiles/${vin}/`;
        const fetchConfig = { method:"put", body: JSON.stringify(putBody), headers: {'Content-Type': 'application/json'} };
        const automobileUpdateResponse = await fetch(automobileUrl, fetchConfig);
        if (automobileUpdateResponse.ok) {
            alert("Automobile sold attribute updated to true!");
        } else {
            console.error('put error:'. automobileUpdateResponse.statusText, automobileUpdateResponse.status);
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/salespeople/';
        const autoUrl = 'http://localhost:8100/api/automobiles/';
        const customerUrl = 'http://localhost:8090/api/customers/';

        const response = await fetch(url);
        const autoResponse = await fetch(autoUrl);
        const customerResponse = await fetch(customerUrl);
        if (response.ok) {
            const data = await response.json();
            setSalesPeople(data.salespersons);
        }
        if (autoResponse.ok) {
            const autoData = await autoResponse.json();
            setAutomobiles(autoData.autos);
        }
        if (customerResponse.ok) {
            const customerData = await customerResponse.json();
            setCustomers(customerData.customers);
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
            data.automobile= automobile;
            data.sales_person = salesPerson;
            data.customer = customer;
            data.price = price;
        const salesUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(salesUrl, fetchConfig);
        if (response.ok) {
            const newSale = await response.json();
            setAutomobile('');
            setSalesPerson('');
            setCustomer('');
            setPrice('');
            alert('sale successfully recorded');
            putAutoSold(newSale.automobile.vin);
        } else {
            console.error('error:', response.statusText, response.status, "if you just created automobile, wait for 1 min for polling, click Create again!");
            alert("If you just created the automobile, wait for 1 min for polling, click Create again!");
        }
    }

    useEffect(() =>{
        fetchData();
        getData();
    }, []);

    const handleAutomobileChange = (event) => {
        const value = event.target.value;
        setAutomobile(value);
    }

    const handleSalesPersonChange = (event) => {
        const value = event.target.value;
        setSalesPerson(value);
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    }


    const filteredAutomobiles = automobiles.filter(automobile => automobile.sold === false);

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div style={{backgroundColor: "rgba(282, 182, 182, .5)"}} className="shadow p-4 mt-4">
                        <h1>Record a new sale</h1>
                        <form onSubmit={handleSubmit} id="create-sale-form">
                            <label htmlFor="automobile">Automobile VIN</label>
                            <div className="mb-3">
                                <select onChange={handleAutomobileChange} value={automobile} required name="automobile" id="automobile">
                                    <option value="">Choose an automobile VIN...</option>
                                    {filteredAutomobiles.map(automobile => {
                                        return (
                                            <option key={automobile.href} value={automobile.vin}>VIN: {automobile.vin} &nbsp;&nbsp; {automobile.year} {automobile.model.manufacturer.name} {automobile.model.name}</option>
                                        );
                                    })}
                                </select>
                            </div>
                            <label htmlFor="salesperson">Salesperson</label>
                            <div className="mb-3">
                                <select onChange={handleSalesPersonChange} value={salesPerson} required name="salesperson" id="salesperson">
                                    <option value="">Choose a salesperson...</option>
                                    {salesPeople.map(salesperson => {
                                        return (
                                            <option key={salesperson.href} value={salesperson.id}>Employee ID: {salesperson.employee_id} &nbsp;&nbsp; Name: {salesperson.first_name} {salesperson.last_name}</option>
                                        );
                                    })}
                                </select>
                            </div>
                            <label htmlFor="customer">Customer</label>
                            <div className="mb-3">
                                <select onChange={handleCustomerChange} value={customer} required name="customer" id="customer">
                                    <option value="">Choose a customer</option>
                                    {customers.map(customer => {
                                        return (
                                            <option key={customer.href} value={customer.id}>Customer ID: {customer.id} &nbsp;&nbsp; Name: {customer.first_name} {customer.last_name}</option>
                                        );
                                    })}
                                </select>
                            </div>
                            <label htmlFor="price">Price</label>
                            <div className="form-floating mb-3">
                                <input value={price} onChange={handlePriceChange} placeholder="price" required type="number" name="price" id="" className="form-control" />
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default SalesForm;
