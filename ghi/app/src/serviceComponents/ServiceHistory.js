import React, {useState, useEffect} from 'react'


function ServicesList() {

    const [appointments, setAppointments] = useState([])

    const [filterVin, setFilterVin] = useState('')
    const [filter, setFilter] =useState(false)

    const handleVinChange = (event) => {
        const value = event.target.value
        setFilterVin(value)
        if (event.target.value === "") {
            setFilter(false)
        }
    }

    const handleSubmit = (event) => {
        event.preventDefault()
        if (filterVin.length === 0) {
            setFilter(false)
        } else {
            setFilter(true)
        }
    }

    const getAppointmentsData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/history/')
        if (response.ok) {
            const appointmentsData = await response.json()
            setAppointments(appointmentsData.appointments)
        } else {
            console.error("An error occurred while fetching the data", response.statusText, response.status)
        }
    }

    useEffect(() => {
        getAppointmentsData()
    }, [])

    return (
        <div style={{backgroundColor: "rgba(282, 182, 182, .8)"}} className="container">
            <div className="my-5 container">
                <div className="row">
                    <h1 style={{textAlign: 'var(--bs-body-text-align)'}}>Service Appointments</h1>
                    <form onSubmit={handleSubmit} id="filter-appointments-form">
                        <div className="form-row">
                            <input onChange={handleVinChange} placeholder="Search by VIN..." type="search" name="vin" id="vin" className="col-11"/>
                            <button className="btn btn-primary">Search</button>
                        </div>
                    </form>
                    <table className="table table-striped">
                        <thead style={{boxSizing:'content-box', height:'80px'}}>
                            <tr>
                                <th>VIN</th>
                                <th>Is VIP?</th>
                                <th>Customer</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Technician</th>
                                <th>Reason</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            {!filter && appointments.map(appointment => {
                                return (
                                    <tr key={ appointment.href }>
                                        <td>{ appointment.vin }</td>
                                        <td>{ appointment.vip?'Yes':'No' }</td>
                                        <td>{ appointment.customer }</td>
                                        <td>{ appointment.date_time.slice(5,7) }/{ appointment.date_time.slice(8,10) }/{ appointment.date_time.slice(0,4) }</td>
                                        <td>{ appointment.date_time.slice(11,13)>=13?appointment.date_time.slice(11,13)-12:appointment.date_time.slice(11,13) }{ appointment.date_time.slice(13,19) } { (appointment.date_time.slice(11,13))>=12?'PM':'AM' }</td>
                                        <td>{ appointment.technician.first_name } { appointment.technician.last_name }</td>
                                        <td>{ appointment.reason }</td>
                                        <td>{ appointment.status }</td>
                                    </tr>
                                )
                            })}
                            {filter && appointments.filter(appointment => appointment.vin===filterVin).map(appointment => {
                                return (               
                                    <tr key={ appointment.href }>
                                        <td>{ appointment.vin }</td>
                                        <td>{ appointment.vip?'Yes':'No' }</td>
                                        <td>{ appointment.customer }</td>
                                        <td>{ appointment.date_time.slice(5,7) }/{ appointment.date_time.slice(8,10) }/{ appointment.date_time.slice(0,4) }</td>
                                        <td>{ appointment.date_time.slice(11,13)>=13?appointment.date_time.slice(11,13)-12:appointment.date_time.slice(11,13) }{ appointment.date_time.slice(13,19) } { (appointment.date_time.slice(11,13))>=12?'PM':'AM' }</td>
                                        <td>{ appointment.technician.first_name } { appointment.technician.last_name }</td>
                                        <td>{ appointment.reason }</td>
                                        <td>{ appointment.status }</td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

export default ServicesList
