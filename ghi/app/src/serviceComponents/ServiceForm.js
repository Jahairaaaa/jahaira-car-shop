import React, { useEffect, useState } from 'react'


function ServiceForm () {

    const [technicians, setTechnicians] = useState([])

    const [vin, setVin] = useState('')
    const [customer, setCustomer] = useState('')
    const [date, setDate] = useState('')
    const [time, setTime] = useState('')
    const [technician, setTechnician] = useState('')
    const [reason, setReason] = useState('')

    const getTechniciansData = async () => {
        const response = await fetch('http://localhost:8080/api/technicians')
        if (response.ok) {
            const techniciansData = await response.json()
            setTechnicians(techniciansData.technicians)
        } else {
            console.error('An error occurred while fetching the technicians data:', response.statusText, response.status)
        }
    }

    const handleVinChange = (event) => {
        const value = event.target.value
        setVin(value)
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value
        setCustomer(value)
    }

    const handleDateChange = (event) => {
        const value = event.target.value
        setDate(value)
    }

    const handleTimeChange = (event) => {
        const value = event.target.value
        setTime(value)
    }

    const handleTechnicianChange = (event) => {
        const value = event.target.value
        setTechnician(value)
    }

    const handleReasonChange = (event) => {
        const value = event.target.value
        setReason(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.vin = vin
        data.customer = customer
        data.date_time = date+":"+time
        data.technician = technician  
        data.reason = reason
        data.status = "created"
        const appointmentsUrl = `http://localhost:8080/api/appointments/`
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {'Content-Type': 'application/json',}
        }
        const newAppointmentResponse = await fetch(appointmentsUrl, fetchConfig)
        if (newAppointmentResponse.ok) {
            setVin('')
            setCustomer('')
            setDate('')
            setTime('')
            setTechnician('')
            setReason('')
            alert("Service Appointment successfully created!")
        } else {
            console.error('error:', newAppointmentResponse.statusText, newAppointmentResponse.status)
            alert('Please verify and correct the VIN')
        }
    }

    useEffect(() =>{
        getTechniciansData()
    }, [])

    let dropdownClasses = 'form-select d-none'
    if (technicians.length > 0) {
      dropdownClasses = 'form-select'
    }

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div style={{backgroundColor: "rgba(282, 182, 182, .5)"}} className="shadow p-4 mt-4">
                        <h1>Create a service appointment</h1>
                        <form onSubmit={handleSubmit} id="create-appointment-form">
                            <label htmlFor="vin">Automobile VIN</label>
                            <div className="form-floating mb-3">
                                <input onChange={handleVinChange} value={vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control"/>
                            </div>
                            <label htmlFor="customer">Customer</label>
                            <div className="form-floating mb-3">
                                <input onChange={handleCustomerChange} value={customer} placeholder="customer" required type="text" name="customer" id="customer" className="form-control"/>
                            </div>
                            <label htmlFor="date">Date</label>
                            <div className="form-floating mb-3">
                                <input onChange={handleDateChange} value={date} placeholder="date" required type="date" name="date" id="date" className="form-control"/>
                            </div>
                            <label htmlFor="time">Time</label>
                            <div className="form-floating mb-3">
                                <input onChange={handleTimeChange} value={time} placeholder="time" required type="time" name="time" id="time" className="form-control"/>
                            </div>
                            <label htmlFor="technician">Technician</label>
                            <div className="mb-3">
                                <select onChange={handleTechnicianChange} value={technician} required name="technician" id="technician" className={dropdownClasses}>
                                    <option value="">Choose a technician</option>
                                    {technicians.map(technician => {
                                        return (
                                            <option key={technician.href} value={technician.id}>Employee ID: {technician.employee_id} &nbsp;&nbsp; Name: {technician.first_name} {technician.last_name}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <label htmlFor="reason">Reason</label>
                            <div className="form-floating mb-3">
                                <input onChange={handleReasonChange} value={reason} placeholder="reason" required type="text" name="reason" id="reason" className="form-control"/>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ServiceForm
