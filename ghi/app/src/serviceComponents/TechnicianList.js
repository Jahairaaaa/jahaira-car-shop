import React, {useState, useEffect} from 'react'


function TechniciansList() {

    const [technicians, setTechnicians] = useState([])

    const getTechniciansData = async () => {
        const response = await fetch('http://localhost:8080/api/technicians/')
        if (response.ok) {
            const techniciansData = await response.json()
            setTechnicians(techniciansData.technicians)
        } else {
            console.error("An error occurred while fetching the data")
        }
    }

    useEffect(() => {
        getTechniciansData()
    }, [])

    return (
        <div style={{backgroundColor: "rgba(282, 182, 182, .8)"}} className="container">
            <div className="my-5 container">
                <div className="row">
                    <h1 style={{textAlign: 'var(--bs-body-text-align)'}}>Technicians</h1>
                    <table className="table table-striped">
                        <thead style={{boxSizing:'content-box', height:'80px'}}>
                            <tr>
                                <th>Employee ID</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            {technicians.map(technician => {
                                return (                 
                                    <tr key={ technician.href }>
                                        <td>{ technician.employee_id }</td>
                                        <td>{ technician.first_name }</td>
                                        <td>{ technician.last_name }</td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

export default TechniciansList
