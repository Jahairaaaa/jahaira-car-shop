import React, { useState } from 'react'


function TechnicianForm () {

    const [first_name, setFirstName] = useState('')
    const [last_name, setLastName] = useState('')
    const [employee_id, setEmployeeId] = useState('')

    const handleFirstNameChange = (event) => {
        const value = event.target.value
        setFirstName(value)
    }

    const handleLastNameChange = (event) => {
        const value = event.target.value
        setLastName(value)
    }

    const handleEmployeeIdChange = (event) => {
        const value = event.target.value
        setEmployeeId(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.first_name = first_name
        data.last_name = last_name
        data.employee_id = employee_id
        const techniciansUrl = `http://localhost:8080/api/technicians/`
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {'Content-Type': 'application/json',}
        }
        const newTechnicianResponse = await fetch(techniciansUrl, fetchConfig)
        if (newTechnicianResponse.ok) {
            setFirstName('')
            setLastName('')
            setEmployeeId('')
            alert("Technician successfully added!")
        } else {
            console.error('error:', newTechnicianResponse.statusText, newTechnicianResponse.status, "employee id taken, try different employee id")
            alert('employee id taken, use different employee id')
        }
    }

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div style={{backgroundColor: "rgba(282, 182, 182, .8)"}} className="shadow p-4 mt-4">
                        <h1>Add a Technician</h1>
                        <form onSubmit={handleSubmit} id="create-technician-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleFirstNameChange} value={first_name} placeholder="first_name" required type="text" name="first_name" id="first_name" className="form-control"/>
                                <label htmlFor="first_name">First name...</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleLastNameChange} value={last_name} placeholder="last_name" required type="text" name="last_name" id="last_name" className="form-control"/>
                                <label htmlFor="last_name">Last name...</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleEmployeeIdChange} value={employee_id} placeholder="employee_id" required type="text" name="employee_id" id="employee_id" className="form-control"/>
                                <label htmlFor="style">Employee ID...</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default TechnicianForm
