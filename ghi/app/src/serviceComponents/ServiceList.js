import React, {useState, useEffect} from 'react'


function ServicesList() {

    const [appointments, setAppointments] = useState([])

    const getAppointmentsData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/')
        if (response.ok) {
            const appointmentsData = await response.json()
            setAppointments(appointmentsData.appointments)
        } else {
            console.error("An error occurred while fetching the data", response.statusText, response.status)
        }
    }

    const handleCancel = async (event) => {
        event.preventDefault()
        const appointmentId = event.target.value
        const appointmentURL = `http://localhost:8080/api/appointments/${appointmentId}/cancel/`
        const putBody = {}
        putBody.status = "cancel"
        const fetchConfig = {
            method: "put",
            body: JSON.stringify(putBody),
            headers: {'Content-Type': 'application/json',}
        }
        const appointmentCancelResponse = await fetch(appointmentURL, fetchConfig)
        if (appointmentCancelResponse.ok) {
            getAppointmentsData()
            alert("service appointment successfully canceled")
        } else {
            console.error("An error occurred while canceling the appointment", appointmentCancelResponse.statusText, appointmentCancelResponse.status)
        }
    }

    const handleFinish = async (event) => {
        event.preventDefault()
        const appointmentId = event.target.value
        const appointmentURL = `http://localhost:8080/api/appointments/${appointmentId}/finish/`
        const putBody = {}
        putBody.status = "finish"
        const fetchConfig = {
            method: "put",
            body: JSON.stringify(putBody),
            headers: {'Content-Type': 'application/json',}
        }
        const appointmentFinishResponse = await fetch(appointmentURL, fetchConfig)
        if (appointmentFinishResponse.ok) {
            getAppointmentsData()
            alert("service appointment successfully finished")
        } else {
            console.error("An error occurred while Finishing the appointment", appointmentFinishResponse.statusText, appointmentFinishResponse.status)
        }
    }

    useEffect(() => {
        getAppointmentsData()
    }, [])

    return (
        <div style={{backgroundColor: "rgba(282, 182, 182, .8)"}} className="container">
            <div className="my-5 container">
                <div className="row">
                    <h1 style={{textAlign: 'var(--bs-body-text-align)'}}>Service Appointments</h1>
                    <table className="table table-striped">
                        <thead style={{boxSizing:'content-box', height:'80px'}}>
                            <tr>
                                <th>VIN</th>
                                <th>Is VIP?</th>
                                <th>Customer</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Technician</th>
                                <th>Reason</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {appointments.filter(appointment => appointment.status === "created").map(appointment => {
                                return (
                                    <tr key={ appointment.href }>
                                        <td>{ appointment.vin }</td>
                                        <td>{ appointment.vip?'Yes':'No' }</td>
                                        <td>{ appointment.customer }</td>
                                        <td>{ appointment.date_time.slice(5,7) }/{ appointment.date_time.slice(8,10) }/{ appointment.date_time.slice(0,4) }</td>
                                        <td>{ appointment.date_time.slice(11,13)>=13?appointment.date_time.slice(11,13)-12:appointment.date_time.slice(11,13) }{ appointment.date_time.slice(13,19) } { (appointment.date_time.slice(11,13))>=12?'PM':'AM' }</td>
                                        <td>{ appointment.technician.first_name } { appointment.technician.last_name }</td>
                                        <td>{ appointment.reason }</td>
                                        <td>
                                            <button onClick={handleCancel} value={appointment.id} className="btn btn-danger">Cancel</button>
                                            <button onClick={handleFinish} value={appointment.id} className="btn btn-success">Finish</button>
                                        </td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

export default ServicesList
