import React, { useState } from 'react';


function CustomersForm() {

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [address, setAddress] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
            data.first_name = firstName;
            data.last_name = lastName;
            data.address = address;
            data.phone_number = phoneNumber;

        const customerUrl = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {
            setFirstName('');
            setLastName('');
            setAddress('');
            setPhoneNumber('');
            alert('customer successfully');
        } else {
            console.error('error:', response.statusText, response.status);
        };

    }
    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }

    const handleAddressChange = (event) => {
        const value = event.target.value;
        setAddress(value);
    }

    const handlePhoneNumberChange = (event) => {
        const value = event.target.value;
        setPhoneNumber(value);
    }

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div style={{backgroundColor: "rgba(282, 182, 182, .5)"}} className="shadow p-4 mt-4">
                        <h1>Add a Customer</h1>
                        <form onSubmit={handleSubmit} id="create-customer-form">
                            <div className="form-floating mb-3">
                                <input value={firstName} onChange={handleFirstNameChange} placeholder="Name" required type="text" name="first_name" id="name" className="form-control" />
                                <label htmlFor="name">First Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={lastName} onChange={handleLastNameChange} placeholder="last name" required type="text" name="last_name" id="" className="form-control" />
                                <label htmlFor="size">Last Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={address} onChange={handleAddressChange} placeholder="address" required type="text" name="color" id="" className="form-control" />
                                <label htmlFor="color">Address</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={phoneNumber} onChange={handlePhoneNumberChange} placeholder="phone_number" type="text" name="phone number" id="" className="form-control" />
                                <label htmlFor='Phone_number'>Phone Number</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default CustomersForm;
