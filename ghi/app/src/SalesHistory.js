import React, { useEffect, useState } from 'react';


function SalesList() {

    const [sales, setSales] = useState([]);

    const [salesPeople, setSalesPeople] = useState([]);

    const [employeeId, setEmployeeId] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/sales/');
        const salesPeopleUrl = `http://localhost:8090/api/salespeople/`;
        const salesPeopleResponse = await fetch(salesPeopleUrl);
        if (response.ok) {
            const salesData = await response.json();
            setSales(salesData.sale);
        } else {
            console.error('An error occurred fetching the data');
        }
        if (salesPeopleResponse.ok) {
            const salesPeopleData = await salesPeopleResponse.json();
            setSalesPeople(salesPeopleData.salespersons);
        } else {
            console.error('An error occurred while fetching the salespeople data:', salesPeopleResponse.statusText, salesPeopleResponse.status);
        }
    };

    useEffect(() => {
        getData();
    }, []);

    const handleSalesPersonChange = (event) => {
        setEmployeeId(event.target.value.split(',')[0]);
        setFirstName(event.target.value.split(',')[1]);
        setLastName(event.target.value.split(',')[2]);
    }

    const filterByName = (first, last, employeeId) => {
        return sales.filter(sale => sale.sales_person.first_name === first).filter(sale => sale.sales_person.last_name === last).filter(sale => sale.sales_person.employee_id.toString() === employeeId);
    }

    const filteredSales = filterByName(firstName, lastName, employeeId);

    let dropdownClasses = 'form-select d-none';
    if (salesPeople.length > 0) {
      dropdownClasses = 'form-select';
    }

    return (
        <div style={{backgroundColor: "rgba(282, 182, 182, .5)"}} className="container">
            <div className="my-5 container">
                <div className="row">
                    <h1>Salesperson History</h1>
                        <div className="mb-3">
                            <select onChange={handleSalesPersonChange}  required name="salesperson" id="salesperson" className={dropdownClasses}>
                                <option value="">Choose a salesperson...</option>
                                {salesPeople.map(salesperson => {
                                    return (
                                        <option key={salesperson.href} value={[salesperson.employee_id, salesperson.first_name, salesperson.last_name]}>Employee ID: {salesperson.employee_id} &nbsp;&nbsp; Name: {salesperson.first_name} {salesperson.last_name}</option>
                                    );
                                })}
                            </select>
                        </div>
                    <table className="table table-striped m-3">
                        <thead style={{boxSizing:'content-box', height:'80px'}}>
                            <tr>
                                <th>Employee Name</th>
                                <th>Employee ID</th>
                                <th>Customer</th>
                                <th>Price</th>
                                <th>Vin</th>
                            </tr>
                        </thead>
                        <tbody>
                            { employeeId.length>0?filteredSales.map(sale => {
                                return (
                                    <tr key={sale.id} value={sale.id}>
                                        <td>{sale.sales_person.first_name}</td>
                                        <td>{sale.sales_person.employee_id}</td>
                                        <td>{sale.customer.first_name}</td>
                                        <td>{sale.price}</td>
                                        <td>{sale.automobile.vin}</td>
                                    </tr>
                                )}):sales.map(sale => {
                                return (
                                    <tr key={sale.href} value={sale.id}>
                                        <td>{sale.sales_person.first_name}</td>
                                        <td>{sale.sales_person.employee_id}</td>
                                        <td>{sale.customer.first_name}</td>
                                        <td>{sale.price}</td>
                                        <td>{sale.automobile.vin}</td>
                                    </tr>
                                )})
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}

export default SalesList;
