import React, { useEffect, useState } from 'react';


function SalesPeopleList() {

    const [salespersons, setSalesPersons] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        if (response.ok) {
            const data = await response.json();
            setSalesPersons(data.salespersons);
        } else {
            console.error('An error occurred fetching the data');
        }
    };

    useEffect(() => {
        getData();
    }, []);

    return (
        <div style={{backgroundColor: "rgba(282, 182, 182, .5)"}} className="container">
            <div className="my-5 container">
                <div className="row">
                    <h1>Sales Personnel</h1>
                    <table className="table table-striped m-3">
                        <thead style={{boxSizing:'content-box', height:'80px'}}>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Employee ID</th>
                            </tr>
                        </thead>
                        <tbody>
                            {salespersons?.map(salesperson => {
                                return (
                                <tr key={salesperson.id} value={salespersons.id}>
                                    <td>{salesperson.first_name}</td>
                                    <td>{salesperson.last_name}</td>
                                    <td>{salesperson.employee_id}</td>
                                </tr>
                            )})}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}

export default SalesPeopleList;
