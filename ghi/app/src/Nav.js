import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <>
      <nav className="navbar navbar-default"></nav>
        <div style={{backgroundColor: "rgba(282, 182, 182, .8)"}} className="container-fluid"></div>
          <div className="navbar-header">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0"/>
              <NavLink style={{paddingRight: "30px", color: "black"}} class="container text-center" className="navbar-brand" to="/">Jahaira's Auto Services</NavLink>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
            </button>
            <div class="dropdown" style={{backgroundColor: "rgba(282, 182, 182, .8)"}} className="container-fluid">
              <a class="btn btn-inverse dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Customers</a>
                <ul class="dropdown-menu">
                  <li><NavLink className="nav-link" to="/customers/">Customers</NavLink></li>
                  <li><NavLink className="nav-link" to="/customers/new/">Create a Customer</NavLink></li>
                </ul>
                
                <a class="btn btn-inverse dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Sales</a>
                <ul class="dropdown-menu">
                    <li><NavLink className="nav-link" to="/sales/new/">Add a Sale</NavLink></li>
                    <li><NavLink className="nav-link" to="/sales/">Sales</NavLink></li>
                    <li><NavLink className="nav-link" to="/sales/history/">Sales History</NavLink></li>
                </ul>

                <a class="btn btn-inverse dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Employee</a>
                <ul class="dropdown-menu">
                  <li><NavLink className="nav-link" to="/technicians/">Technicians</NavLink></li>
                  <li><NavLink className="nav-link" to="/technicians/new/">Add a Technician</NavLink></li>
                  <li><NavLink className="nav-link" to="/salespeople/">Salespeople</NavLink></li>
                  <li><NavLink className="nav-link" to="/salespeople/new/">Add a Salesperson</NavLink></li>
                </ul>

                <a class="btn btn-inverse dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Appointments</a>
                <ul class="dropdown-menu">
                  <li><NavLink className="nav-link" to="/services/">Service Appointments</NavLink></li>
                  <li><NavLink className="nav-link" to="/services/new/">Create a Service Appointment</NavLink></li>
                  <li><NavLink className="nav-link" to="/services/history/">Service History</NavLink></li>
                </ul>

                <a class="btn btn-inverse dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Automobiles</a>
                <ul class="dropdown-menu">
                  <li><NavLink className="nav-link" to="/manufacturers/new/">Create a Manufacturer</NavLink></li>
                  <li><NavLink className="nav-link" to="/manufacturers/">Manufacturers</NavLink></li>
                  <li><NavLink className="nav-link" to="/models/new/">Create a Model</NavLink></li>
                  <li><NavLink className="nav-link" to="/models/">Models</NavLink></li>
                  <li><NavLink className="nav-link" to="/automobiles/">Automobiles</NavLink></li>
                  <li><NavLink className="nav-link" to="/automobiles/new/">Create an Automobile</NavLink></li>
                  </ul>
                  <a class="btn btn-inverse fixed-right" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Login</a>
                  <a class="btn btn-inverse" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Register</a>
            </div>
          </div>
    </>
  )
}

export default Nav;